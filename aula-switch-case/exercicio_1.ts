/*
Escreva um programa que pergunte ao usuário qual o seu nível de conhecimento em TypeScript e exiba uma mensagem de acordo com o nível escolhido:
*/

namespace exercicio_1
{
    let nivel_de_conhecimento: number;
    nivel_de_conhecimento = 4;

    switch (nivel_de_conhecimento)
    {
        case 1: console.log("Baixo");
                break;
        case 2: console.log("Medio");
                break;
        case 3: console.log("Avançado");
                break;
        default: console.log("Nem te conto");
        
    }
}