//Crie um array com 4 objetos, cada um representando um livro com as propriedades titulo e autor. Em seguida, use o método map() para criar um novo array contendo apenas os títulos dos livros.
namespace exercicio_3
{
    /*let livros: any[] = [{Titulo: "diario de um banana", Autor: "Jeff Kinney"}, {Titulo: "A Turma do Dudão Chegou!", Autor: "Jairo Alves da Silva"}, {Titulo: "a ladra de livros", Autor: "Markus Zusak"}, {Titulo: "livro dos insutos", Autor: "leo lins"}];
    let Autor = livros.map(function(livros)
    {
        return livros.Autor;
    });
    let Titulo = livros.map(function(livros)
    {
        return livros.Titulo;
    });

    console.log(Autor);
    console.log(Titulo);*/

    let livro: any[] = [{titulo: "T1", autor: "A1"}, {titulo: "oioo", autor: "Autor 3"}, {titulo: "oi", autor: "Autor 3"}, {titulo: "T4", autor: "A4"}]
    
    let autor3= livro.filter(function(autor) {
        return autor.autor === "Autor 3";
    });

    console.log(autor3);

    let titulos = autor3.map(function(titulo){
        return titulo.titulo
    });

    console.log(titulos);
}