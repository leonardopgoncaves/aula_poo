namespace exercicio_6
{
    interface Aluno
    {
        nome: string,
        idade: number,
        notas: number[];
    }
    const alunos: Aluno[] = [
        {nome: "Aluno1", idade: 20, notas:[4, 7, 8]},
        {nome: "Aluno2", idade: 1000, notas:[1, 2, 4]},
        {nome: "Aluno3", idade: 30, notas:[10, 2, 5]},
        {nome: "Aluno4", idade: 18, notas:[4, 9, 1]},
        {nome: "Aluno5", idade: -1, notas:[10, 10, 10]}
    ]
    /*alunos.forEach((Aluno) => {
        console.log("-----------------------------------------------------");
        
        console.log(Aluno);
        
    })*/

    alunos.forEach((Aluno) => {
        let media: number;
        media = (Aluno.notas[0] + Aluno.notas[1] + Aluno.notas[2]) / 3;

        if (media >= 7) {
            console.log("---------------------------");

            console.log(`A media do aluno: ${Aluno.notas} é igual ${media} e esta aprovado`);
            
        }
        else{
            console.log("---------------------------");

            console.log(`A media do aluno: ${Aluno.notas} é igual ${media} e esta reprovado`);
        }
        console.log("---------------------------");
        
        console.log(media);
        
    })
}