//Crie um array com 5 números. Em seguida, use um loop for para iterar sobre o array e exibir a soma de todos os números.

namespace exercicio
{
    let notas: number[] = [10, 8, 6, 2, 9];

    let somaNotas: number = 0;

    for (let i = 0; i < notas.length; i++) 
    {
        somaNotas += notas[i]
    }
    console.log(somaNotas);

    let multi: number = 1;
    for (let i = 0; i < notas.length; i++) {
        multi *= notas[i];
        
    }
    console.log(multi);
}