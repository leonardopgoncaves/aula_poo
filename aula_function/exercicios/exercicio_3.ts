//Crie uma função que receba um array de números como parâmetro e retorne a soma desses números.

namespace exercicio_3 
{
    function resultado(numero:number[]) {
        let soma: number = 0;
        for (let i = 0; i < numero.length; i++) {
            soma += numero[i];
        }
        return soma
    }
    console.log(resultado([1, 2, 3, 4]));
}