//Dada a matriz abaixo, crie um código que calcule e mostre a média aritmética dos valores de cada linha:

namespace exercicio_2
{
    let matriz: number[][] = [
        [1, 2, 3],
        [4, 5, 6],
        [7, 8, 9]
    ];

    let media: number;
    let soma = 0;
    matriz.forEach(row => {
        row.forEach(col => {
        soma += col;
        });
    });
    media = soma / 3;

    console.log(media);
}