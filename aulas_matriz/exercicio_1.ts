//Crie uma matriz 3x3 com números aleatórios entre 0 e 10. Em seguida, crie um código que calcule e mostre o maior valor encontrado na matriz.
namespace exercicio_1
{
    let matriz: number [][] = Array.from({
        length: 3}, () => Array(3). fill(0))

        for (let i = 0; i < matriz.length; i++) {
            for(let j = 0; j < matriz[i].length;j++){
                matriz[i][j] = Math.floor(Math.random() * 11)
            }
        }
        
        let maiorValor = matriz[0][0];
    for (let i = 0; i < matriz.length; i++) {
        for (let j = 0; j < 3; j++) {
        if (matriz[i][j] > maiorValor) {
        maiorValor = matriz[i][j] = Math.floor(Math.random() * 11)
        }
    }
}


console.log("Matriz:");
console.table(matriz);
console.log(`Maior valor encontrado na matriz: ${maiorValor}`);
}