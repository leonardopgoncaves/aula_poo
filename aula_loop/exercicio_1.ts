//Escreva um programa TypeScript que imprima todos os números primos de 1 a 50 usando a função

namespace exercicio1
{
    let numero, aux: number;
    let primos: boolean;

    numero = 0
    while (numero <= 53)
    {
        aux = 2;
        primos = true;
        while (aux < numero)
        {
            if(numero % aux == 0)
            {
                primos = false;
                break;
            }
            aux ++
        }
        if (primos == true && numero > 1)
        {
            console.log(numero);
        }
    }
}